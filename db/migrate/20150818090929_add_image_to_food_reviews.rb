class AddImageToFoodReviews < ActiveRecord::Migration
  def change
    add_column :food_reviews, :image, :string
  end
end

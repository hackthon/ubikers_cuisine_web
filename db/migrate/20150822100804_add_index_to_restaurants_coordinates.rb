class AddIndexToRestaurantsCoordinates < ActiveRecord::Migration
  def change
		add_index :restaurants, [:lat, :lng], unique: true
  end
end

class CreateCircles < ActiveRecord::Migration
  def change
    create_table :circles do |t|
      t.float :lat
      t.float :lng

      t.timestamps null: false
    end
		add_index :circles, [:lat, :lng]
  end
end

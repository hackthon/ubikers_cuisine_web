class AddIndexToRestaurants < ActiveRecord::Migration
  def change
		add_index :restaurants, :address, unique: true
  end
end

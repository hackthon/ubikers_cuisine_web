class AddCoordinatesToRestaurants < ActiveRecord::Migration
  def change
    add_column :restaurants, :lat, :Float
    add_column :restaurants, :lng, :Float
		add_index :restaurants, [:lat, :lng], unique: true
  end
end

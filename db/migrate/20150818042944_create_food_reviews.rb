class CreateFoodReviews < ActiveRecord::Migration
  def change
    create_table :food_reviews do |t|
      t.text :content
      t.references :user, index: true, foreign_key: true
      t.references :restaurant, index: true, foreign_key: true

      t.timestamps null: false
    end
		add_index :food_reviews, [:user_id, :restaurant_id, :created_at]
  end
end

require 'WEBrick'
data = File.read("restaurants.txt").split("\n").to_a
restaurants = []
(0..(data.size - 1)).step(3) do |i|
	restaurants.push(Restaurant.new(name: data[i], address: data[i + 1], url: data[i + 2]))
end

restaurants.each do |restaurant|
	query = "http://maps.googleapis.com/maps/api/geocode/json?address=#{restaurant.address}&sensor=false"
	#puts query
	query.force_encoding("binary")
	query = WEBrick::HTTPUtils.escape(query)
	result = JSON.parse(open(query).read)
	sleep(1)
	restaurant.lat = result["results"][0]["geometry"]["location"]["lat"]
	restaurant.lng = result["results"][0]["geometry"]["location"]["lng"]
	restaurant.save unless Restaurant.find_by(lat: restaurant.lat, lng: restaurant.lng)
end

'''
query = "http://maps.googleapis.com/maps/api/geocode/json?address=台北市信義區忠孝東路四段553巷28號&sensor=false"
query.force_encoding("binary")
query = WEBrick::HTTPUtils.escape(query)
result = JSON.parse(open(query).read)
puts result["results"].count
'''

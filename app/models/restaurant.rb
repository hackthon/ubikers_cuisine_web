class Restaurant < ActiveRecord::Base
	searchkick(autocomplete: [:name], word_start: [:name]) if Rails.env.development?

	has_many :food_reviews, dependent: :destroy
	accepts_nested_attributes_for :food_reviews
	#validates :food_reviews, length: {minimum: 1}
	validates :name, presence: true
	validates :address, presence: true, uniqueness: true
	validate :check_location_validity

	private
		
		def check_location_validity
			if lat == 25.0169826 and lng == 121.4627868
				errors.add(:address, "should be a valid address")
			end
		end
end

class RestaurantsController < ApplicationController
	require 'WEBrick' if Rails.env.development?
	before_action :logged_in_user, only: [:new, :create, :edit, :update, :destroy]
	before_action :admin_user, only: [:edit, :update, :destroy]
		
	def show
		@restaurant = Restaurant.find(params[:id])
		@food_reviews = @restaurant.food_reviews.paginate(page: params[:page])
	end

	def index
		if params[:query].present?
			@restaurants = Restaurant.search(params[:query], fields: [{name: :word_start}])
		else
			@restaurants = Restaurant.all
		end
	end

	def create
		restaurant = Restaurant.new(restaurant_params)
		@food_review = current_user.food_reviews.build(food_review_params)

		query = "http://maps.googleapis.com/maps/api/geocode/json?address=#{restaurant.address}&sensor=false"
		query.force_encoding("binary")
		query=WEBrick::HTTPUtils.escape(query)
		result = JSON.parse(open(query).read)
		restaurant.lat = result["results"][0]["geometry"]["location"]["lat"]
		restaurant.lng = result["results"][0]["geometry"]["location"]["lng"]
		@restaurant = Restaurant.find_by(lat: restaurant.lat, lng: restaurant.lng)

		if !@restaurant
			if restaurant.save
				@food_review.restaurant_id = restaurant.id
				if @food_review.save
					flash[:info] = "Restaurant and Review created!"
					redirect_to restaurant
				else
					render "new"
				end
			else
				render "new"
			end
		else
			@food_review.restaurant_id = @restaurant.id
			if @food_review.save
				flash[:info] = "Review created!"
				redirect_to @restaurant
			else
				render "new"
			end
		end
	end

	def edit
		@restaurant = Restaurant.find(params[:id])
	end

	def update
		@restaurant = Restaurant.find(params[:id])
		if @restaurant.update_attributes(restaurant_params)
			flash[:success] = "Restaurant updated"
			redirect_to @Restaurant
		else
			render 'edit'
		end
	end

	def destroy
		Restaurant.find(params[:id]).destroy
		flash[:success] = "Restaurant deleted"
		redirect_to restaurants_url
	end

	def autocomplete
		render json: Restaurant.search(params[:query], fields: [{name: :word_start}]).map(&:name)
	end

	private

		def restaurant_params
			params.require(:restaurant).permit(:name, :address)
		end

		def food_review_params
			params.require(:food_review).permit(:content, :image, :image_cache)
		end

		def admin_user
			redirect_to root_url unless current_user.admin?
		end
end

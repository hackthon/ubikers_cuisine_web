class MapController < ApplicationController
	require 'open-uri'

	@@count = 0

  def show		
		station_result = JSON.parse(open("http://data.taipei/opendata/datalist/apiAccess?scope=resourceAquire&rid=ddb80380-f1b3-4f8e-8016-7ed9cba571d5").read)

		stations = []
		station_result["result"]["results"].each do |station|
			stations.push(Station.new(lat: station["lat"], lng: station["lng"], sna: station["sna"], 
																sbi: station["sbi"], bemp: station["bemp"], act: station["act"]))
		end
	
		url_header = "http://chart.apis.google.com/chart?chst=d_map_spin&chld=0.7|0|"
		@hash = Gmaps4rails.build_markers(stations) do |station, marker|
			marker.lat station.lat
			marker.lng station.lng
			@station = station
			
			if station.act == "0"
				marker.picture url: url_header + "696969|15|b|", width: 26, height: 47
			elsif station.sbi == "0"
				marker.picture url: url_header + "FF8C00|15|b|x", width: 26, height: 47
			elsif station.bemp == "0"
				marker.picture url: url_header + "FF0000|15|b|F", width: 26, height: 47
			else
				marker.picture url: url_header + "7CFC00|15|b|o", width: 26, height: 47
			end
			marker.infowindow render_to_string(partial: "stats")
		end

		@hash_restaurant = Gmaps4rails.build_markers(Restaurant.all) do |restaurant, marker|
			@restaurant = restaurant
			marker.lat restaurant.lat
			marker.lng restaurant.lng
			marker.picture url: url_header + "0000FF|15|b|∆", width: 26, height: 47
			marker.infowindow render_to_string(partial: "restaurant")
		end
	
		@circles = Circle.all
  end

  def save
		circle = Circle.new(lat: params[:total_changes][:lat], lng: params[:total_changes][:lng], 
												rad: params[:total_changes][:rad])
		circle.save

	  #puts "lat: " +  params[:total_changes][:lat] + " lng: " +  params[:total_changes][:lng]
	  render "show" 
  end
end

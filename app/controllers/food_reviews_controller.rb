class FoodReviewsController < ApplicationController
	before_action :logged_in_user, only: [:create, :destroy]
	before_action :correct_user, only: :destroy

	def new
		@restaurant = Restaurant.new
		@food_review = current_user.food_reviews.build
	end

	def destroy
		@food_review.destroy
		flash[:success] = "Review deleted"
		redirect_to request.referrer || root_url
	end

	private
		
		def correct_user
			@food_review = current_user.food_reviews.find_by(id: params[:id])
			redirect_to root_url if @food_review.nil?
		end
end

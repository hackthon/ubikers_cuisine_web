class ApplicationMailer < ActionMailer::Base
  default from: "noreply@ubikers-cuisine.com"
  layout 'mailer'
end
